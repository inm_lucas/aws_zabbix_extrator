from pyzabbix import ZabbixAPI
from datetime import datetime
import psycopg2
import settings as s
import pandas as pd
from io import StringIO
import pytz
from datetime import timedelta
import boto3

import time

def getitems(ids, zapi):
    items = zapi.item.get(hostids = ids, 
                      output = ['key_', 'name', 'host', 'itemid', 'hostid', 'lastvalue', 'lastclock'],
                      filter = {'status': '0'})
    items_df = pd.DataFrame(items)
    return items_df

def group_parser(group):
	return ', '.join([g['name'] for g in group])

def floatcasting(value):
    try:
        return float(value)
    except:
        return None

def get_primary_keys(tablename, db):
    c = db.cursor()
    sql = "select indexdef from pg_indexes where tablename = '%s';" % tablename
    c.execute(sql)
    result = c.fetchall()[0][0]
    rfields = result.split('(')[1].strip(')').split(',')
    fields = [field.strip().strip('"') for field in rfields]
    return fields

def lambda_handler(event, handler):
	start = time.clock()
	zapi = ZabbixAPI(s.con, timeout = 10000)
	zapi.login(s.user, s.paswd)
	hosts = zapi.host.get(
						output = ['host', 'hostid'],
						filter = {'status': '0'},
						selectGroups='extend'
						)
	hosts_df = pd.DataFrame(hosts)
	hostids = list(hosts_df['hostid'].values)

	items_df = [getitems(hostids[idx:idx+100], zapi) for idx in range(0, len(hostids), 100)]
	items_df = pd.concat(items_df)

	tz = pytz.timezone('America/Sao_Paulo')
	now = datetime.now(tz).strftime('%Y-%m-%d')
	delay = (datetime.now(tz) - timedelta(days = 7)).strftime('%Y-%m-%d')

	host_items = pd.merge(hosts_df, items_df, how = 'right', on  = 'hostid')
	host_items['lastclock'] = host_items['lastclock'].apply(lambda t: datetime.fromtimestamp(int(t), tz))
	host_items['lastvalue'] = host_items['lastvalue'].apply(lambda v: floatcasting(v))
	host_items['itemid'] = host_items['itemid'].astype(int)
	host_items['hostid'] = host_items['hostid'].astype(int)

	#host_items.set_index('lastclock', inplace = True)
	#print(host_items.between_time(start_time = delay, end_time = now).head())
	#host_items = host_items.between_time(start_time = delay, end_time = now)
	ts = datetime.now().timestamp()

	host_items = host_items[host_items['lastclock'] > delay]

	host_items.dropna(inplace = True)
	host_items['groups'] = host_items['groups'].apply(lambda g: group_parser(g))

	snippet_items = [65785, 34951, 34961, 65787, 34957, 65778]

	host_items = host_items[host_items['itemid'].isin(snippet_items)]
	#host_items = host_items.reset_index()
	host_items = host_items.rename(columns = {'lastclock': 'clock', 'lastvalue': 'value'})

	host_items = host_items.rename(columns = {'hostid': 'host_id',
											'itemid': 'item_id',
											'host': 'host_name',
											'key_': 'item_key',
											'name': 'item_name',
											'groups': 'group_name',
											}) 

	host_items = host_items[['group_name', 'host_id', 'host_name', 'item_id', 'item_key',
							'item_name', 'clock', 'value']]
	#host_items['clock'] = host_items['clock'].apply(lambda t: t.strftime('%Y-%m-%d %H:%M:%S'))

	csv_buffer = StringIO()
	host_items.to_csv(csv_buffer, sep = ';', index = False)#, compression = 'gzip')

	filename = 'sodexo_zabbixcollect_'+str(int(ts))+'.csv'

	#Armazenamento no S3
	s3_resource = boto3.resource('s3', aws_access_key_id=s.akey, aws_secret_access_key=s.skey)
	s3_resource.Object('analyticscoe','sodexo/prod/extrator_sodexo_zabbix_prod/'+filename).put(Body=csv_buffer.getvalue())

	#Armazenamento no RedShift
	temp_tablename = 'temp_%s' % str(int(ts))

	db = psycopg2.connect(host = s.rscon,
							port = s.port,
							user = s.rsuser,
							database = s.database,
							password = s.rspasswd)

	s3path = 's3://analyticscoe/sodexo/prod/extrator_sodexo_zabbix_prod/'+filename
	tablename = 'sodexo_zabbix_real_demo'
	primary_keys = get_primary_keys(tablename, db)
	equals_clause = '{dest}.%s = {src}.%s'
	join_clause = ' AND '.join([equals_clause % (pk, pk) for pk in primary_keys])
	join_clause = join_clause.format(dest=tablename, src=temp_tablename)

	upsert_qry = """\
	CREATE TEMPORARY TABLE {src} (LIKE {dest});
	COPY {src} FROM '{s3path}' 
	CREDENTIALS 'aws_access_key_id={access_key};aws_secret_access_key={secret_key}' 
	DELIMITER AS '{delimiter}' IGNOREHEADER 1;
	BEGIN;
	LOCK {dest};
	DELETE FROM {dest} USING {src} WHERE {join_clause};
	INSERT INTO {dest} SELECT * FROM {src};
	END;
	""".format(dest=tablename,
				src=temp_tablename,
				s3path=s3path,
				join_clause=join_clause,
				access_key=s.akey,
				secret_key=s.skey,
				delimiter=';')

	c = db.cursor()
	c.execute(upsert_qry)
	db.commit()
	db.close()


start = time.clock()
events = []
handler = []
lambda_handler(events, handler)
print ('Tempo de Execução:', time.clock() - start)
