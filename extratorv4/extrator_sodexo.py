from pyzabbix import ZabbixAPI
from datetime import datetime
import psycopg2
import settings as s
import pandas as pd
from io import StringIO
import pytz
from datetime import timedelta
from functools import reduce
import boto3

import time

def getitems(ids, zapi):
    items = zapi.item.get(hostids = ids, 
                      output = ['key_', 'name', 'host', 'itemid', 'hostid', 'lastvalue', 'lastclock'],
                      #filter = {'status': '0'}
                    )
    #print(items)
    #print('\n')
    items_df = pd.DataFrame(items)
    return items_df

def upsert_redshift(csv_buffer, tablename):

    s3_resource = boto3.resource('s3', aws_access_key_id=s.akey, aws_secret_access_key=s.skey)
    s3_resource.Object('analyticscoe','sodexo/buffer_hom/buffer.csv').put(Body=csv_buffer.getvalue())

    s3path = 's3://analyticscoe/sodexo/buffer_hom/buffer.csv'
    #Armazenamento no RedShift
    temp_tablename = 'temp_%s' % str(int(datetime.today().timestamp()))

    db = psycopg2.connect(host = s.rscon,
                            port = s.port,
                            user = s.rsuser,
                            database = s.database,
                            password = s.rspasswd)

    s3path = s3path
    primary_keys = get_primary_keys(tablename, db)
    equals_clause = '{dest}.%s = {src}.%s'
    join_clause = ' AND '.join([equals_clause % (pk, pk) for pk in primary_keys])
    join_clause = join_clause.format(dest=tablename, src=temp_tablename)
    
    upsert_qry = """\
    CREATE TEMPORARY TABLE {src} (LIKE {dest});
    COPY {src} FROM '{s3path}' 
    CREDENTIALS 'aws_access_key_id={access_key};aws_secret_access_key={secret_key}' 
    DELIMITER AS '{delimiter}' IGNOREHEADER 1;
    BEGIN;
    LOCK {dest};
    DELETE FROM {dest} USING {src} WHERE {join_clause};
    INSERT INTO {dest} SELECT * FROM {src};
    END;
    """.format(dest=tablename,
                src=temp_tablename,
                s3path=s3path,
                join_clause=join_clause,
                access_key=s.akey,
                secret_key=s.skey,
                delimiter=';')

    c = db.cursor()
    c.execute(upsert_qry)
    db.commit()
    db.close()

def group_parser(group):
	return ', '.join([g['name'] for g in group])

def groupid_parser(group):
	return ', '.join([g['groupid'] for g in group])

def floatcasting(value):
    try:
        return float(value)
    except:
        return None

def get_primary_keys(tablename, db):
    c = db.cursor()
    sql = "select indexdef from pg_indexes where tablename = '%s';" % tablename
    c.execute(sql)
    result = c.fetchall()[0][0]
    rfields = result.split('(')[1].strip(')').split(',')
    fields = [field.strip().strip('"') for field in rfields]
    return fields

def create_dataframe_row_parser(g):
    g_df = pd.DataFrame(g)[['groupid', 'name']]
    g_df.rename(columns = {'name': 'group_name', 'groupid': 'group_id'}, inplace = True)
    return g_df

def expand_df_on_groups(df):
    wiseit_hosts = [10629,10628,10857,10654,10665,10519,10518,10854,
                    10855,10856,10667,10668,10911,10655,10656,10653]
    pedeFacil_hosts = [10160,10814,10815,10161,11415,10162,11485,11486,11487,
                       10106,10128,11358,11369,11411,11387,11355,11364,11354,
                       11372,11408,11388,10489,11201,11202,11203,11200]
    exp_df = pd.DataFrame(df['groups'])[['groupid', 'name']]
    exp_df = exp_df.rename(columns = {'groupid': 'group_id', 'name': 'group_name'})
    info = df.drop('groups').to_frame().T.reset_index().drop('index', axis = 1)
    data = pd.concat([exp_df,info], axis = 1)
    data = data.fillna(method = 'ffill')
  
    if data['host_id'].values[0] in wiseit_hosts:
        #data = data.append(pd.DataFrame({'service_id': '1001'},
        #index = [0]), ignore_index = True)
        #data = data.fillna(method = 'ffill')
        data['service_id'] = 1001
        data['service_name'] = 'WiseIT'
    elif data['host_id'].values[0] in pedeFacil_hosts:
        #data = data.append(pd.DataFrame({'service_id': '1000'},
        #index = [0]), ignore_index = True)
        #data = data.fillna(method = 'ffill')
        data['service_id'] = 1000
        data['service_name'] = 'Pede Facil'
       
    
    return data

def lambda_handler(event, handler):

    start = time.clock()
    zapi = ZabbixAPI(s.con, timeout = 10000)
    zapi.login(s.user, s.paswd)

    """
    Pede Fácil
    10160 - App.PedeFacil.galileo010
    10814 - App.PedeFacil-via IP
    10815 - App.PedeFacil-External
    10161 - App.PedeFacil.galileo011
    11415 - App.PedeFacil.galileo012
    10162 - App.PedeFacil.kepler020	
    11485 - App.PedeFacil.kepler021
    11486 - App.PedeFacil.kepler022	
    11487 - App.PedeFacil.kepler023
    10106 - App.PedeFacil.URL-prod
    10128 - App.PedeFacil.URL-homolog
    11358 - AppServer.PedeFacil.kepler020
    11369 - AppServer.PedeFacil.kepler021
    11411 - AppServer.PedeFacil.kepler022
    11387 - AppServer.PedeFacil.kepler023
    11355 - AppServer.PedeFacilINT.kepler020
    11364 - AppServer.PedeFacilINT.kepler021
    11354 - AppServer.PedeFacilRel.kepler020
    11372 - AppServer.PedeFacilRel.kepler021
    11408 - AppServer.PedeFacilRel.kepler022
    11388 - AppServer.PedeFacilRel.kepler023
    10489 - galileo105
    11201 - kepler020
    11202 - kepler022    
    11203 - kepler021
    11200 - kepler023

    WiseIT
    10629 - cebrsvc-spwi01
    10628 - cebrsvc-spwi02
    10857 - cebrsvc-spwi03
    10654 - App.WiseIT.URL-prod
    10665 - App.WiseIT.URL-prod_External
    10519 - CEBRSVC-SPDB01B
    10518 - CEBRSVC-SPDB01A
    10854 - App.WiseIT-Server01
    10855 - App.WiseIT-Server02
    10856 - App.WiseIT-Server03
    10667 - App.WiseIT.cebrsvc-rjwi11a
    10668 - App.WiseIT.URL-homolog
    10911 - App.WiseIT.cebrsvc-rjwi11b
    10655 - App.WiseIT.URL.cebrsvc-spwi01
    10656 - App.WiseIT.URL.cebrsvc-spwi02
    10653 - cebrsvc-rjwi11b
    """

    wiseit_hosts = [10629,10628,10857,10654,10665,10519,10518,10854,
                    10855,10856,10667,10668,10911,10655,10656,10653]
    pedeFacil_hosts = [10160,10814,10815,10161,11415,10162,11485,11486,11487,
                       10106,10128,11358,11369,11411,11387,11355,11364,11354,
                       11372,11408,11388,10489,11201,11202,11203,11200]

    hosts = zapi.host.get(hostids =[10629,10628,10857,10654,10665,10519,10518,10854,
                    10855,10856,10667,10668,10911,10655,10656,10653,10160,10814,10815,10161,11415,10162,11485,11486,11487,
                       10106,10128,11358,11369,11411,11387,11355,11364,11354,
                       11372,11408,11388,10489,11201,11202,11203,11200],
                    output = ['host', 'hostid'],
                    filter = {'status': '0'},
                    selectGroups='extend'
                    )
    
    hosts_df = pd.DataFrame(hosts)
    hostids = list(hosts_df['hostid'].values)

    print(hostids)
    
    items_df = [getitems(hostids[idx:idx+100], zapi) for idx in range(0, len(hostids), 100)]
    items_df = pd.concat(items_df)

    print(items_df.hostid.unique())
    tz = pytz.timezone('America/Sao_Paulo')
    now = datetime.now(tz).strftime('%Y-%m-%d')
    delay = (datetime.now(tz) - timedelta(days = 7)).strftime('%Y-%m-%d')

    host_items = pd.merge(hosts_df, items_df, how = 'right', on  = 'hostid')
    host_items['lastclock'] = host_items['lastclock'].apply(lambda t: datetime.fromtimestamp(int(t), tz))
    host_items['lastvalue'] = host_items['lastvalue'].apply(lambda v: floatcasting(v))
    host_items['itemid'] = host_items['itemid'].astype(int)
    host_items['hostid'] = host_items['hostid'].astype(int)

    print(host_items.hostid.unique())
    host_items = host_items[host_items['lastclock'] > delay]
    
    print(host_items.hostid.unique())
    host_items.dropna(inplace = True)

    host_items = host_items.rename(columns = {'lastclock': 'clock', 'lastvalue': 'value'})
    host_items = host_items.rename(columns = {'hostid': 'host_id',
                                        'itemid': 'item_id',
                                        'host': 'host_name',
                                        'key_': 'item_key',
                                        'name': 'item_name',
                                        }) 
    print("Data Prep", time.clock() - start)

    start = time.clock()

    """
    #DIM group
    groups = hosts_df[['groups']].copy()

    dfs = [create_dataframe_row_parser(g) for g in groups['groups']]    

    g_dfs = pd.concat(dfs, axis = 0)    
    g_dfs = g_dfs.drop_duplicates()
    g_dfs = g_dfs.append([{'group_name': 'WiseIT', 'group_id': '1001'}], ignore_index = True)
    g_dfs = g_dfs.append([{'group_name': 'Pede Fácil', 'group_id': '1000'}], ignore_index = True)
    
    #DIM Services

    #service = hosts_df[['groups']].copy()
    services = [df.tail(1) for df in dfs]
    service = pd.concat(services, axis = 0)
    service = service.rename(columns = {'group_name': 'service_name', 'group_id': 'service_id'})
    service = service.drop_duplicates()
    service = service.append([{'service_name': 'WiseIT', 'service_id': '1001'}], ignore_index = True)
    service = service.append([{'service_name': 'Pede Fácil', 'service_id': '1000'}], ignore_index = True)
    
    s_dfs = pd.concat([expand_df_on_groups(df)
    for index, df in host_items.drop(labels = ['clock', 'value'], axis = 1).iterrows()])
    filter_df = pd.merge(s_dfs, g_dfs, how = 'left', on = 'group_id')

    filter_df = pd.merge(filter_df, service, how = 'left',
    left_on = 'group_id', right_on = 'service_id')
    """
    filter_df = pd.concat([expand_df_on_groups(df)
    for index, df in host_items.drop(labels = ['clock', 'value'], axis = 1).iterrows()])

    filter_df = filter_df[['service_id', 'service_name', 'group_id',
    'group_name', 'host_id', 'host_name', 'item_id', 'item_key', 'item_name']]

    filter_df['service_id'] = filter_df['service_id'].fillna(-1)
    #filter_df['service_name'] = filter_df['service_name'].fillna('Não Definido')
    filter_df['item_id'] = filter_df['item_id'].astype(int)
    filter_df['host_id'] = filter_df['host_id'].astype(int) 
    filter_df['service_id'] = filter_df['service_id'].astype(int) 
    filter_df['group_id'] = filter_df['group_id'].astype(int)
    filter_df = filter_df.reset_index().drop('index', axis = 1) 

    ##print(filter_df[['service_id', 'service_name', 'group_id', 'group_name', 'item_key']].tail())

    buffer_filter = StringIO()  
    filter_df.to_csv(buffer_filter, sep = ';', index = False)

    upsert_redshift(buffer_filter, 'filter_sodexo_zabbix_hom')  
    print("Dim Filter", time.clock() - start)

    #FACT 
    start = time.clock()
    rs_fact = host_items[['clock', 'groups', 'host_id', 'item_id', 'value']]
    rs_fact['group_id'] = rs_fact['groups'].apply(lambda g: groupid_parser(g))
    rs_fact.loc[rs_fact.host_id.isin(pedeFacil_hosts), 'service_id'] = '1000'
    rs_fact.loc[rs_fact.host_id.isin(wiseit_hosts), 'service_id'] = '1001'
    #rs_fact['service_id'] = rs_fact['group_id'].apply(lambda g: g.split(',')[-1])
    rs_fact = rs_fact.drop(labels = ['groups'], axis = 1)
    rs_fact = rs_fact[['service_id', 'group_id',
    'host_id', 'item_id', 'clock', 'value']]

    buffer_fact = StringIO() 
    rs_fact.to_csv(buffer_fact, sep = ';', index = False)

    upsert_redshift(buffer_fact, 'fact_sodexo_zabbix_real_hom')
    print("Dim Facts", time.clock() - start)

    start = time.clock()
    #RAW File

    host_items['groups'] = host_items['groups'].apply(lambda g: group_parser(g))
    #host_items['clock'] = host_items['clock'].apply(lambda t: t.strftime('%Y-%m-%d %H:%M:%S'))

    ts = datetime.now().timestamp()

    csv_buffer = StringIO()
    host_items.to_csv(csv_buffer, sep = ';', index = False)#, compression = 'gzip')

    filename = 'sodexo_zabbixcollect_'+str(int(ts))+'.csv'

    #Armazenamento no S3
    s3_resource = boto3.resource('s3', aws_access_key_id=s.akey, aws_secret_access_key=s.skey)
    s3_resource.Object('analyticscoe','sodexo/homologação/extrator_sodexo_zabbix_hom'+filename).put(Body=csv_buffer.getvalue())
    print("Dim RAW", time.clock() - start)
    
start = time.clock()
events = []
handler = []
lambda_handler(events, handler)
print('\n')
print ('Tempo de Execução:', time.clock() - start)