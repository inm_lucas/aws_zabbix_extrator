import psycopg2
from datetime import datetime
import settings_alelo as s
import time
import pandas as pd
import os
import boto
import pytz
from boto.s3.key import Key

import time

import json

def upload_to_s3(aws_access_key_id, aws_secret_access_key, file, bucket, key, callback=None, md5=None, reduced_redundancy=False, content_type=None):
    """
    Uploads the given file to the AWS S3
    bucket and key specified.

    callback is a function of the form:

    def callback(complete, total)

    The callback should accept two integer parameters,
    the first representing the number of bytes that
    have been successfully transmitted to S3 and the
    second representing the size of the to be transmitted
    object.

    Returns boolean indicating success/failure of upload.
    """
    try:
        size = os.fstat(file.fileno()).st_size
    except:
        # Not all file objects implement fileno(),
        # so we fall back on this
        file.seek(0, os.SEEK_END)
        size = file.tell()

    conn = boto.connect_s3(aws_access_key_id, aws_secret_access_key)
    bucket = conn.get_bucket(bucket, validate=True)
    k = Key(bucket)
    k.key = key
    if content_type:
        k.set_metadata('Content-Type', content_type)
    sent = k.set_contents_from_file(file, cb=callback, md5=md5, reduced_redundancy=reduced_redundancy, rewind=True)

    # Rewind for later use
    file.seek(0)

    if sent == size:
        return True
    return False

def infer_freq(df):
    """
    Infers frequency string from dataframe/series index.
    
    Parameters
    ----------
    df : DataFrame or Series
       Pandas objects with a DatetimeIndex.
    """
    idx = df.index[-100:]
    
    freq = pd.infer_freq(idx, warn=False)
    if freq is not None:
        return freq
    
    period = idx.to_series().diff().dt.round('10s').value_counts().index[0]
    if period >= pd.Timedelta('28 D'):
        return 'M' if idx[0].day >= 28 else 'MS'
    else:
        return pd.tseries.frequencies.to_offset(period).freqstr

def align_timestamps(df, freq=None):
    """
    Aligns timestamps to pretty multiples of frequency.
    
    Parameters
    ----------
    df : DataFrame or Series
       Pandas objects with a DatetimeIndex.
    freq : str
       Frequency to align to.  Defaults to infered frequency.
    """
    if freq is None:
        freq = infer_freq(df)
    idx = pd.date_range(df.index[0].round(freq), df.index[-1].round(freq), freq=freq)
    tol = pd.date_range('2016-07-01', freq=freq, periods=2).to_series().diff().iloc[-1] / 2
    return df.reindex(idx, method='nearest', tolerance=tol)

def lambda_handler(event, handler):
    #Conexão com o Zabbix via Read Replica
    con = psycopg2.connect(host = s.connection,
                           port = s.port,
                           user = s.user,
                           database = s.database,
                           password = s.passwd)


    now = datetime.now()
    table = 'history_'+str(now.year)+str(now.month)

    sql = """
            SELECT {0}.clock as clock, 
            {0}.value as value, items.key_ as item_key,
            items.itemid as itemid,
            hosts.host as host
            from {0}, items, hosts
            where
                {0}.itemid = items.itemid
                and
                items.hostid = hosts.hostid
                and
                (items.key_ like '%cpu%idle' or items.key_ like '%memory%used%')
                and
                clock > extract(epoch from now() - interval '5 minute')
    """.format(table)
    history = pd.read_sql(sql, con)
    print(len(history['itemid'].unique()))
    tz = pytz.timezone('America/Sao_Paulo')
    history['clock'] = history['clock'].apply(lambda c: datetime.fromtimestamp(c, tz))
    history['clock'] = history['clock'].apply(lambda c: c.strftime('%Y-%m-%d %H:%M:%S'))
    print(history['itemid'].value_counts())

    for doc in history.to_dict('records'):
        print(doc)
        fname = 'zabbix_'+str(doc['itemid'])+'_'+str(doc['clock'])+'.json'
        fout = open('/tmp/'+fname, 'w')
        json_file = json.dumps(doc)
        fout.write(json_file)
        fout.close()
        fin = open('/tmp/'+fname)

        upload_to_s3(s.akey,s.skey,
        fin, bucket = 'analyticscoe', key = 'alelo/poc/' + fname)
        #return