from pyzabbix import ZabbixAPI
from datetime import datetime
import settings as s
import pandas as pd
from io import StringIO
import pytz
from datetime import timedelta
import boto3

def getitems(ids, zapi):
    items = zapi.item.get(hostids = ids, 
                      output = ['key_', 'name', 'host', 'itemid', 'hostid', 'lastvalue', 'lastclock'],
                      filter = {'status': '0'})
    items_df = pd.DataFrame(items)
    return items_df

def group_parser(group):
	return ', '.join([g['name'] for g in group])

def floatcasting(value):
    try:
        return float(value)
    except:
        return None

def lambda_handler(event, handler):
	zapi = ZabbixAPI(s.con, timeout = 10000)
	zapi.login(s.user, s.paswd)
	hosts = zapi.host.get(
	                  output = ['host', 'hostid'],
	                  filter = {'status': '0'},
	                  selectGroups='extend'
	                 )
	hosts_df = pd.DataFrame(hosts)
	hostids = list(hosts_df['hostid'].values)

	items_df = [getitems(hostids[idx:idx+100], zapi) for idx in range(0, len(hostids), 100)]
	items_df = pd.concat(items_df)

	tz = pytz.timezone('America/Sao_Paulo')
	now = datetime.now(tz).strftime('%Y-%m-%d')
	delay = (datetime.now() - timedelta(days = 7)).strftime('%Y-%m-%d')

	host_items = pd.merge(hosts_df, items_df, how = 'right', on  = 'hostid')
	host_items['lastclock'] = host_items['lastclock'].apply(lambda t: datetime.fromtimestamp(int(t)))
	host_items['lastvalue'] = host_items['lastvalue'].apply(lambda v: floatcasting(v))
	host_items.set_index('lastclock', inplace = True)
	host_items = host_items[slice(delay,now)]
	host_items.dropna(inplace = True)
	host_items['groups'] = host_items['groups'].apply(lambda g: group_parser(g))

	csv_buffer = StringIO()
	host_items.to_csv(csv_buffer, sep = ';')

	ts = datetime.now().timestamp()
	filename = 'sodexo_zabbixcollect_'+str(int(ts))+'.csv'

	s3_resource = boto3.resource('s3', aws_access_key_id=s.akey, aws_secret_access_key=s.skey)
	s3_resource.Object('analyticscoe','sodexo/extract_data/'+filename).put(Body=csv_buffer.getvalue())